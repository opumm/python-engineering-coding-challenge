Python Coding Challenge with Django
=======================

Hi, this is the Digital Healthcare Solution's python coding challenge. This challenge is designed to test your Python and Django skills.

Requirements
============

- Completed using Python `v3.6 or higher` and all code must be annotated with type hints from the standard library `typing` module.
- Runs on docker and the application can be started with a single command `docker-compose up`
- The running application can be reached in the browser at *[docker host]:8080*
- The application is delivered with a sufficient admin reachable at *[docker host]:8080/admin*
- Once test is completed please share the public Git repository URL to us so they can review your work

Scenario
========

This is an outstanding time we are going through. Our health and economic system are collapsing. People are dying. We couldn't save many of them just because of the lack of preparation. For taking enough precautious measures we need proper information. 


Task brief
====
We need to build a web application. where people can get information about the coronavirus outbreak. So that we can take precautions.

We can get our data from THE GLOBAL HEALTH OBSERVATORY.
https://www.who.int/data/gho/info/gho-odata-api

We need to visualize 
 - Number of people affected [detected, recovered, died]
 - Trend of new cases
 - Trend of death toll

Filters 
 - Continent
 - Country
 - Age group



_Bonus:_ Try to limit external API calls by scheduling a long task with celery.

Restrictions
============

None! Use whatever tools / third party libraries you feel are necessary to complete the task.